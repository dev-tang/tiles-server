# tiles-server

#### 介绍
基于Rust语言开发的瓦片地图服务器，可用于离线地图开发。examples中提供了基于OpenLayers调用的栗子。

#### 软件架构
依赖：rusqlite、base64、actix-web、actix-files、serde


#### 安装教程

1.  git clone https://gitee.com/dev-tang/tiles-server.git
2.  cd tiles-server
3.  cargo run

#### 使用说明

1.  MBtiles放在data目录，访问地址：`/mbtiles/filename/z/x/y.(png|jpg)`
2.  图片瓦片放在tiles目录，访问地址：`/tiles/z/x/y.(png|jpg)`

#### 举个栗子
1. 运行tiles-server：`cargo run`
2. 在浏览器中访问瓦片：`http://localhost:3000/mbtiles/test/18/214103/148004.png`
3. 在OpenLayers中调用瓦片，在浏览器中打开：`examples/ol.html`，出现如下图结果，则表示一切正常

![](./examples/result.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
