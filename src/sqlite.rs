use rusqlite::{params, Connection, Result};

/// 根据参数从mbtiles文件中查询一条数据，并返回
/// `tiles` mbtiles文件名
/// `z` 地图的zone
/// `x` 地图的x坐标
/// `y` 地图的y坐标
pub fn select_tile_data(tiles: &str, z: i32, x: i32, y: i32) -> Result<(Vec<u8>)> {
    let conn = Connection::open(format!("./data/{}.mbtiles", tiles))?;

    let mut stmt = conn.prepare("SELECT `tile_data` FROM `tiles` WHERE `zoom_level` = ? AND `tile_column` = ? AND `tile_row` = ? LIMIT 1")?;

    let tile_data: Vec<u8> = stmt.query_row(params![z, x, y], |row| {
        Ok(row.get(0)?)
    })?;

    Ok(tile_data)
}