use std::io;
use tiles::*;

fn main() -> io::Result<()> {
    println!("{}", "tiles-server v1.0.1");
    println!("{}", "listening 0.0.0.0:3000");

    server::start("0.0.0.0:3000")?;

    Ok(())
}
